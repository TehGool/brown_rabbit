window.onload = function () {
  setInitialSlide();
  resetSearchTerm();
  setArticlePage(1);
};

var currentArticlePage = 1;
const totalArticlePage = $(".pagination li").length - 2;
const articlesPerPage = 3;

function resetSearchTerm() {
  $("#searchTerm").val("");
}

function setInitialSlide() {
  var slides = $(".carousel-item").length;
  var random = Math.floor(Math.random() * slides);
  $(".carousel-item").eq(random).addClass("active");
}

function setArticlePage(selectedPage) {
  $(".content__article").hide();
  $(".content__article").each(function (numberOfArticles) {
    if (
      numberOfArticles >= articlesPerPage * (selectedPage - 1) &&
      numberOfArticles < articlesPerPage * selectedPage
    )
      $(this).show();
  });
  $("#selectedPage").text("Page " + selectedPage + " of " + totalArticlePage);
  currentArticlePage = selectedPage;
}

$(function () {
  var selectPage = function () {
    $(".page-item").removeClass("active");
    $(this).parent(2).addClass("active");
    setArticlePage(parseInt($(this).text()));
  };

  $(".pagination li a:not('#nextPage')").on("click", selectPage);
});

$(function () {
  var nextPage = function () {
    nextPage = currentArticlePage + 1;
    if (nextPage <= totalArticlePage) {
      setArticlePage(nextPage);
      $(".pagination li.active")
        .removeClass("active")
        .next()
        .addClass("active");
    }
  };

  $(".pagination li a#nextPage").on("click", nextPage);
});

$(function () {
  var mark = function () {
    var searchTerm = $("#searchTerm").val();
    var options = {
      element: "mark",
      separateWordSearch: false,
      accuracy: "partialy",
      diacritics: true,
      caseSensitive: false,
    };

    $(".content").unmark({
      done: function () {
        $(".content").mark(searchTerm, options);
      },
    });
  };

  $("#searchTerm").on("input", mark);

  //documentation: https://markjs.io/
});

$(function () {
  var readMore = function () {
    if ($(this).text() === "Read more") {
      $(this).parent().css("height", "100%");
      $(this).text("Read less");
    } else if ($(this).text() === "Read less") {
      $(this).parent().css("height", "150px");
      $(this).text("Read more");
    }
  };

  $("button.btn.btn-sm").click(readMore);
});
